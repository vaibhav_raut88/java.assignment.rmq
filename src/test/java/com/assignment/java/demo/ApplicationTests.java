package com.assignment.java.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.assignment.java.rmq.model.SignupData;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
class ApplicationTests {

	protected MockMvc mvc;

	@Test
	void contextLoads() throws Exception { 

		MvcResult mvcResult = mvc.perform( MockMvcRequestBuilders
				.post("/signup")
				.content(asJsonString(new SignupData("John Alan Doe", "992 9293 992")))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
