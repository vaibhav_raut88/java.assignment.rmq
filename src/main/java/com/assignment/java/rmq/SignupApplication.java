package com.assignment.java.rmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SignupApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SignupApplication.class);

	public static void main(String[] args) throws InterruptedException {
		LOGGER.debug("Staring Signup Application!");
		
		SpringApplication.run(SignupApplication.class, args).close();;
	}

}
