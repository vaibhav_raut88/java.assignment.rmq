package com.assignment.java.rmq.processor;

import com.assignment.java.rmq.model.SignupProcessedData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.assignment.java.rmq.model.SignupData;

public class SignupDataProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignupDataProcessor.class);

	public static SignupProcessedData process(SignupData signupData) throws Exception {
		SignupProcessedData signinData = new SignupProcessedData();
		
		LOGGER.debug("Processing Signup data: " + signupData);
		
		// Process Name
		String[] spaceSeparatedWords = signupData.getFullName().split(" ");
		
		if(spaceSeparatedWords == null || spaceSeparatedWords.length < 2) {
			LOGGER.error("Invalid Name: " + signupData.getFullName());
			throw new Exception("Invalid Name!");
		}

		signinData.setFirstName(spaceSeparatedWords[0]);
		
		signinData.setLastName(spaceSeparatedWords[spaceSeparatedWords.length - 1]);
		
		// Process Phone
		String phone = signupData.getPhone().replaceAll(" ", "");
		if(!phone.startsWith("+33")) {
			phone = "+33" + phone;
		}
		
		signinData.setPhone(phone);
		
		return signinData;
	}
}
