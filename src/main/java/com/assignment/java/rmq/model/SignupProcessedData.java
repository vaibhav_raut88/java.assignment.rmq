package com.assignment.java.rmq.model;

import com.fasterxml.jackson.annotation.JsonClassDescription;

@JsonClassDescription
public class SignupProcessedData {

	private String firstName;
	private String lastName;
	private String phone;
	
	public SignupProcessedData() {
		super();
	}

	public SignupProcessedData(String firstName, String lastName, String phone) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
	}
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "SignupProcessedData [firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone + "]";
	}
}
