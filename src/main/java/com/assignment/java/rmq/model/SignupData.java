package com.assignment.java.rmq.model;

import com.fasterxml.jackson.annotation.JsonClassDescription;

@JsonClassDescription
public class SignupData {

	private String fullName;
	private String phone;
	
	public SignupData() {
		super();
	}
	public SignupData(String fullName, String phone) {
		super();
		this.fullName = fullName;
		this.phone = phone;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "SignupData [fullName=" + fullName + ", phone=" + phone + "]";
	}
}
