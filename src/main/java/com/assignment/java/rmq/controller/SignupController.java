package com.assignment.java.rmq.controller;

import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.java.rmq.model.SignupData;
import com.assignment.java.rmq.service.sender.SignupMessageSender;

@RestController
public class SignupController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SignupController.class);

	@Autowired
	private SignupMessageSender rabbitMQSender;

	@SuppressWarnings("rawtypes")
	@PostMapping("/")
	public ResponseEntity signup(@RequestParam SignupData signUpData) {

		try {
			validateSignupData(signUpData);
			rabbitMQSender.send(signUpData);
		} catch(Exception e) {
			LOGGER.error("Error occured during Sending message on queue: " + e.toString());
			return new ResponseEntity(HttpStatus.BAD_REQUEST);			
		}

		return new ResponseEntity(HttpStatus.OK);
	}

	private void validateSignupData(SignupData signUpData) throws Exception {
		if(Strings.isNullOrEmpty(signUpData.getFullName()) || Strings.isNullOrEmpty(signUpData.getPhone()) ||
				signUpData.getFullName().length() < 3 || !signUpData.getFullName().contains(" ")) {

			LOGGER.error("Invalid Input Data!");
			throw new Exception("Invalid Input Data!");
		}
	}
}
