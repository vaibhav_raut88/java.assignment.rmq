package com.assignment.java.rmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class RabbitMQConfig {

	@Value("${signup.rabbitmq.queue}")
	private String queueName;

	@Value("${signup.rabbitmq.exchange}")
	private String exchange;

	@Value("${signup.rabbitmq.routingkey}")
	private String routingkey;

  @Bean
  Queue queue() {
    return new Queue(queueName, false);
  }

  @Bean
  TopicExchange exchange() {
    return new TopicExchange(exchange);
  }

  @Bean
  Binding binding(Queue queue, TopicExchange exchange) {
    return BindingBuilder.bind(queue).to(exchange).with(routingkey);
  }

  @Bean
  public MessageConverter jsonMessageConverter() {
	  return new Jackson2JsonMessageConverter();
  }
	
  @Bean
  public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory) {
	  final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
	  rabbitTemplate.setMessageConverter(jsonMessageConverter());
	  return rabbitTemplate;
  }	

  @Bean
  SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                           MessageListenerAdapter listenerAdapter) {
      SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
      container.setConnectionFactory(connectionFactory);
      container.setQueueNames(queueName);
      container.setDefaultRequeueRejected(false);
      container.setMissingQueuesFatal(false);
      return container;
  }
  
}
