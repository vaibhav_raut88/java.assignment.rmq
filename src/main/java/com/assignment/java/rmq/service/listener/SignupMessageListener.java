package com.assignment.java.rmq.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.assignment.java.rmq.model.SignupProcessedData;
import com.assignment.java.rmq.model.SignupData;
import com.assignment.java.rmq.processor.SignupDataProcessor;

@Component
public class SignupMessageListener implements MessageListenerI {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignupMessageListener.class);

	@RabbitListener(queues = "${signup.rabbitmq.queue}")
	@Override
	public void recievedMessage(Object data) throws Exception {

		SignupData signupData = (SignupData) data;
		LOGGER.debug("Recieved Message From RabbitMQ: " + signupData);

		try {
			// Process the Signup Info
			SignupProcessedData signinData = SignupDataProcessor.process(signupData);
			
			LOGGER.info("User: " + signinData.getFirstName() + " " + signinData.getLastName() 
					+ " with phone " + signinData.getPhone() + " has just signed up!");

		} catch (Exception e) {
			// TODO Auto-generated catch block 
			LOGGER.error(e.toString());
			throw e;
		}
	}
}