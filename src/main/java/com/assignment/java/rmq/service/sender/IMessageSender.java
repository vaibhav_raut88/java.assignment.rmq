package com.assignment.java.rmq.service;

public interface MessageSenderI {
	public void send(Object data);
}
