package com.assignment.java.rmq.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SignupMessageSender implements MessageSenderI {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignupMessageSender.class);

	@Autowired
	private AmqpTemplate amqpTemplate;

	@Value("${signup.rabbitmq.exchange}")
	private String exchange;

	@Value("${signup.rabbitmq.routingkey}")
	private String routingkey;

	@Override
	public void send(Object data) {
		LOGGER.debug("Putting message to Message Queue: " + data); 
		
		amqpTemplate.convertAndSend(exchange, routingkey, data);
		
		
	}
}